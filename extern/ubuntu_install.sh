# set -e
sudo apt-get update
sudo apt-get install -y wget python-pip
sudo apt-get install -y imagemagick tesseract-ocr
# ocropus
# sudo apt-get install curl python-tables imagemagick python-opencv firefox
# sudo apt-get install imagemagick tesseract-ocr
# sudo apt-get install tesseract-ocr-eng
# sudo apt-get install cuneiform
# sudo apt-get install ghostscript

# gcc, needed by pyyaml, Pillow
sudo apt-get install -y python-imaging python-yaml
# libjpeg, zlib needed by Pillow
sudo apt-get install -y lib1g-dev zlib1g-dev libpng-dev
# try apt-get python-yaml?
sudo apt-get install -y build-essential
sudo pip install -r extern/py_requirements.txt
